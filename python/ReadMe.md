<!--
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
-->

This folder contains supplementary python routines. They are not
needed to run the NetLogo model, but can be consulted to understand
how input data was preprocessed.
