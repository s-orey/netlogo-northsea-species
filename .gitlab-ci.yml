# SPDX-FileCopyrightText: 2022-2023 Helmholtz-Zentrum hereon GmbH
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
#
include:
  - ".gitlab/license.yaml"
  - ".gitlab/docker.yaml"
  - ".gitlab/joss.yaml"
  - ".gitlab/odd.yaml"
  - ".gitlab/pandoc.yaml"
  - ".gitlab/zenodo.yaml"
  - ".gitlab/readthedocs.yaml"
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
stages:
  - lint
  - run
  - test
  - deploy
  - release
unit-tests:
  image: comses/netlogo
  stage: test
  script:
    - echo "Trying to run in working directory and headless mode"
    - echo "Java HOME is ${JAVA_HOME}"
    - cd ${CI_PROJECT_DIR}/netlogo
    - sed  -i 's/^NetLogo 6..../NetLogo 6.2.2/g' vinos.nlogo
    - 'bash /opt/netlogo/netlogo-headless.sh --model ./vinos.nlogo --setup-file ./behavior/unit-tests.xml
      --experiment "unit-tests" --table -

      '
  allow_failure: false
  needs: []
  timeout: 10 min
create-map:
  image: comses/netlogo
  stage: run
  script:
    - echo "Trying to run in working directory and headless mode"
    - echo "Java HOME is ${JAVA_HOME}"
    - cd ${CI_PROJECT_DIR}/netlogo
    - sed  -i 's/^NetLogo 6..../NetLogo 6.2.2/g' vinos.nlogo
    - 'bash /opt/netlogo/netlogo-headless.sh --model ./vinos.nlogo --setup-file ./behavior/effort-map.xml
      --experiment "effort-map" --table - '
  artifacts:
    paths:
      - "./netlogo/results/effort*png"
  needs: []
  allow_failure: true
  interruptible: true
  timeout: 10 min

reproducibility:
  image: comses/netlogo
  stage: run
  script:
    - echo "Trying to run in working directory and headless mode"
    - echo "Java HOME is ${JAVA_HOME}"
    - cd ${CI_PROJECT_DIR}/netlogo
    - sed  -i 's/^NetLogo 6..../NetLogo 6.2.2/g' vinos.nlogo
    - 'bash /opt/netlogo/netlogo-headless.sh --model ./vinos.nlogo --setup-file ./behavior/reproducibility.xml
      --experiment "reproducibility" --table - '
    - md5sum results/reproducibility_1_effort_h_0010_20210411-0000.png > results/reproducibility_1_effort_h_0010_20210411-0000.png.md5
  artifacts:
    paths:
      - "./netlogo/results/reproducibility_1_effort_h_0010_20210411-0000.png.md5"
  needs: []
  allow_failure: false
  interruptible: true
  timeout: 10 min

profile:
  image: comses/netlogo
  stage: run
  script:
    - echo "Trying to run in working directory and headless mode"
    - echo "Java HOME is ${JAVA_HOME}"
    - cd ${CI_PROJECT_DIR}/netlogo
    - sed  -i 's/^NetLogo 6..../NetLogo 6.2.2/g' vinos.nlogo
    - 'bash /opt/netlogo/netlogo-headless.sh --model ./vinos.nlogo --setup-file ./behavior/profile.xml
      --experiment "profile" --table - '
  artifacts:
    paths:
      - "./netlogo/results/profiler_data.csv"
  needs: []
  allow_failure: true
  interruptible: true
  timeout: 10 min

# Job to deploy the web page of the project, currently showing
# the generated pdf papers for JOSS and COMSES.
pages:
  stage: deploy
  script:
    - cp -r .gitlab/public .
    - cp ./doc/odd/odd.pdf public/odd.pdf
    - cp ./doc/joss/paper.pdf public/joss.pdf
  #    - cp ./netlogo/results/effort*png public/effort.png
  artifacts:
    paths:
      - public/
  needs:
    - joss-inara
    - odd-paper
    - create-map

# Automated job for software security, provided by GitLab
sast:
  stage: test

release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG # Run this job when a tag is created
  script:
    - echo "Running release_job"
  release: # See https://docs.gitlab.com/ee/ci/yaml/#release for available properties
    tag_name: "$CI_COMMIT_TAG"
    description: "$CI_COMMIT_TAG"
