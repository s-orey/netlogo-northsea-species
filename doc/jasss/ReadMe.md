<!--
SPDX-FileCopyrightText:  2020 Giangiacomo Bravo
SPDX-License-Identifier: LPPL-1.0
-->

A CLASS AND TEMPLATE FOR JASSS ARTICLES v.0.6, 2020-11-19
Author: Giangiacomo Bravo <giangiacomo.bravo@lnu.se>

https://www.overleaf.com/project/65c244c6bc044c0cb9d89db7
