% SPDX-FileCopyrightText: 2023 Universität Hamburg
% SPDX-FileCopyrightText: 2023-2024 Helmholtz-Zentrum hereon GmbH
% SPDX-FileCopyrightText: 2023 Hochschule Bremerhaven
% SPDX-License-Identifier: CC0-1.0

@book{BenDor2019,
  title={Agent-Based Modeling of Environmental Conflict and Cooperation},
  author={BenDor, Todd K. and Scheffran, J{\"u}rgen},
  edition=1,
  year={2019},
  publisher={CRC},
  address={Boca Raton},
  doi={10.1201/9781351106252},
  issn={978-1-138-47603-5},
}

@article{Grimm2010,
author = {Grimm, Volker and Berger, Uta and DeAngelis, Donald L. and Polhill, J. Gary and Giske, Jarl and Railsback, Steven F.},
doi = {10.1016/j.ecolmodel.2010.08.019},
issn = {03043800},
journal = {Ecological Modelling},
keywords = {Model description,Model formulation,Model replication,Scientific communication,Standardization},
month = {nov},
number = {23},
pages = {2760--2768},
title = {{The ODD protocol: A review and first update}},
url = {https://linkinghub.elsevier.com/retrieve/pii/S030438001000414X},
volume = {221},
year = {2010}
}

@article{Grimm2020,
abstract = {The Overview, Design concepts and Details (ODD) protocol for describing Individual-and Agent-Based Models (ABMs) is now widely accepted and used to document such models in journal articles. As a standardized document for providing a consistent, logical and readable account of the structure and dynamics of ABMs, some research groups also find it useful as a workflow for model design. Even so, there are still limitations to ODD that obstruct its more widespread adoption. Such limitations are discussed and addressed in this paper: the limited availability of guidance on how to use ODD; the length of ODD documents; limitations of ODD for highly complex models; lack of sufficient details of many ODDs to enable reimplementation without access to the model code; and the lack of provision for sections in the document structure covering model design ratio-nale, the model's underlying narrative, and the means by which the model's fitness for purpose is evaluated. We document the steps we have taken to provide better guidance on: structuring complex ODDs and an ODD summary for inclusion in a journal article (with full details in supplementary material; Table 1); using ODD to point readers to relevant sections of the model code; update the document structure to include sections on model rationale and evaluation. We also further advocate the need for standard descriptions of simulation experiments and argue that ODD can in principle be used for any type of simulation model. Thereby ODD would provide a lingua franca for simulation modelling.},
author = {Grimm, Volker and Railsback, Steven F. and Vincenot, Christian E. and Berger, Uta and Gallagher, Cara and Deangelis, Donald L. and Edmonds, Bruce and Ge, Jiaqi and Giske, Jarl and Groeneveld, J{\"{u}}rgen and Johnston, Alice S.A. and Milles, Alexander and Nabe-Nielsen, Jacob and Polhill, J. Gareth and Radchuk, Viktoriia and Rohw{\"{a}}der, Marie Sophie and Stillman, Richard A. and Thiele, Jan C. and Ayll{\'{o}}n, Daniel},
doi = {10.18564/jasss.4259},
issn = {14607425},
journal = {Journal of Artifical Societies and Social Simulation},
number = {2},
title = {{The ODD protocol for describing agent-based and other simulation models: A second update to improve clarity, replication, and structural realism}},
volume = {23},
year = {2020}
}

@online{Hillewaert2005,
  title = {Shrimp, plaice, sole},
  author = {Hans Hillewaert},
  year = {2005. CC-by-SA 4.0},
  urldate = {2023-02-02},
  url = {https://www.wikimedia.org},
}

@masterthesis{Oerey2023,
  type = {PhD thesis},
  author = {Serra \"Orey},
  title  = {Spatio temporal diversity in German brown shrimp fishery},
  school  = {Carl von Ossietzky Universit\"at Oldenburg},
  note   = "",
  month  = "",
  year   = {2023},
  annote = ""
}

@incollection{Scheffran2016,
  title={From a Climate of Complexity to Sustainable Peace: Viability Transformations and Adaptive Governance in the Anthropocene},
  author={Scheffran, J{\"u}rgen},
  booktitle={Handbook on Sustainability Transition and Sustainable Peace},
  editor={Brauch, Hans Gunter and Oswald Spring, {\"U}rsula and Grin, John and Scheffran, J{\"u}rgen},
  year={2016},
  publisher={Springer},
  address={Cham},
  pages={249--264},
  volume={10},
  series={Hexagon Series on Human and Environmental Security and Peace},
  doi={10.1007/978-3-319-43884-9_13}
}

@article{Scheffran2000,
  title={The Dynamic Interaction Between Economy and Ecology: Cooperation, Stability and Sustainability for a Dynamic-Game Model of Resource Conflicts},
  author={Scheffran, J{\"u}rgen},
  journal={Mathematics and Computers in Simulation},
  volume={53},
  year={2000},
  pages={371--380}
}

@online{Sochat2018,
  author = {Sochat, Vanessa},
  title = {{Validation for Journal of Open Source Software}},
  url = {https://vsoch.github.io/2018/submission-joss/},
  date = {2018-09-28},
  urldate = {2023-03-20},
  organization = {@vsoch (blog)}
}

@manual{Wilensky1999,
  title={{NetLogo}},
  author={Wilensky, Uri},
  year={1999},
  organization={Center for Connected Learning and Computer-Based Modeling, Northwestern University},
  address={Evanston, IL}
}

@online{FSF2023,
  title = {{REUSE Software}},
  organization = {{Free Software Foundation Europe e.V.}},
  url = {https://reuse.software},
  urldate = {2023-04-02},
  year = {2023}
}

@online{Stackoverflow2018,
title = {{NetLogo how to add a legend?}},
author = {@arn, Javier Sandoval, Luke C},
url = {https://stackoverflow.com/questions/51328633/netlogo-how-to-add-a-legend},
urldate = {2023-04-03},
year = {2018},
organization = {Stackoverflow},
}

@article{Letschert2023,
abstract = {Worldwide, fisheries face the consequences of climate change and compete with expanding human activities at sea, which may trigger unforeseen reactions of fishers. Hence, knowledge on drivers of fishing behavior is crucial for management and needs to be integrated in resource management policies. In this study, we identify factors influencing fishing activity of North Sea demersal fleets. First, we explore drivers of the North Sea demersal fisheries in scientific literature. Subsequently, we study the effects of identified drivers on the spatio-temporal dynamics of German demersal fisheries using boosted regression trees (BRT), a supervised machine learning technique. An exploratory literature review revealed a lack of studies incorporating biophysical, economic and socio-cultural fishing drivers in a single quantitative analysis. Our BRT analysis contributed to filling this research gap and highlighted the importance of biophysical drivers such as temperature, salinity, and bathymetry for fishing behavior. Contrary to findings of previous studies, our empirical analysis identified quotas and market prices to be irrelevant, except for low brown shrimp prices, which counter-intuitively increased fishing effort. Moreover, economic and socio-cultural variables influencing brown shrimp fishing effort differed from the other fleets, especially determined by increased effort on workdays and reduced effort when fuel prices were high. Our findings provide key information for marine spatial planning and supports the integration of fishing fleet behavior into policies.},
author = {Letschert, Jonas and Kraan, Casper and M{\"{o}}llmann, Christian and Stelzenm{\"{u}}ller, Vanessa},
doi = {10.1016/j.ocecoaman.2023.106543},
file = {:Users/Lemmen/Downloads/Letschert2023_etal_oceancoastalmanagement.pdf:pdf},
issn = {09645691},
journal = {Ocean and Coastal Management},
keywords = {Boosted regression tree,Fishing behavior,Fishing drivers,Marine spatial planning,Resource management},
mendeley-groups = {ABM},
number = {March},
title = {{Socio-ecological drivers of demersal fishing activity in the North Sea: The case of three German fleets}},
volume = {238},
year = {2023}
}

@article{dulvy2008climate,
  title={Climate change and deepening of the North Sea fish assemblage: a biotic indicator of warming seas},
  author={Dulvy, Nicholas K and Rogers, Stuart I and Jennings, Simon and Stelzenm{\"u}ller, Vanessa and Dye, Stephen R and Skjoldal, Hein R},
  journal={Journal of Applied Ecology},
  volume={45},
  number={4},
  doi = {10.1111/j.1365-2664.2008.01488.x},
  pages={1029--1039},
  year={2008},
  publisher={Wiley Online Library}
}

@article{stelzenmuller2022plate,
  title={From plate to plug: the impact of offshore renewables on European fisheries and the role of marine spatial planning},
  author={Stelzenm{\"u}ller, V and Letschert, J and Gimpel, A and Kraan, C and Probst, WN and Degraer, S and D{\"o}ring, R},
  journal={Renewable and Sustainable Energy Reviews},
  volume={158},
  pages={112108},
  doi = {10.1016/j.rser.2022.112108},
  year={2022},
  publisher={Elsevier}
}


@inbook{Döring2020,
abstract = {This chapter provides an overview of the small-scale fishing sector in Germany. The small-scale fleet is defined here as including vessels up to 24 m in length that are usually family owned and operate in the Baltic and North Seas. The fleet comprises of beam trawlers, cutter trawlers and vessels employing passive gears. Target species are brown shrimp and flatfish in the North Sea and herring, cod, flatfish and certain freshwater species in the Baltic Sea. Beam trawlers only fish in the coastal North Sea, whereas the small gill netters are based exclusively along the Baltic coast. The North and Baltic Seas have very different physical conditions. The North Sea has substantial tidal influence and normal marine salinity, while the Baltic Sea has minimal tidal influence and brackish water. After the introduction of the Common Fisheries Policy in 1983 (in East Germany 1990), individual quotas were issued. Most small-scale fishers are organised by producer organisations. They can be subject to additional conservation regulations which only apply to coastal zones. The increasing demand for fish stemming from fisheries which are certified as sustainable might impose further restrictions on small-scale fisheries. Most fishers employing passive gear have been in a steadily precarious economic situation due to very small quotas. Thus, their number is continuously decreasing. The shrimp and cutter fleets are in a healthier economic state, owing to their fishing of unregulated species (shrimp) or having the possibility to fish in both the Baltic and North Seas (cutter trawler fleet). Despite these advantages, investment in new vessels is extremely scarce in German small-scale fisheries.},
address = {Cham},
author = {D{\"{o}}ring, Ralf and Berkenhagen, J{\"{o}}rg and Hentsch, Solveig and Kraus, Gerd},
booktitle = {Small-Scale Fisheries in Europe: Status, Resilience and Governance},
doi = {10.1007/978-3-030-37371-9_23},
editor = {Pascual-Fern{\'{a}}ndez, Jos{\'{e}} J and Pita, Cristina and Bavinck, Maarten},
isbn = {978-3-030-37371-9},
pages = {483--502},
publisher = {Springer International Publishing},
title = {{Small-Scale Fisheries in Germany: A Disappearing Profession?}},
url = {https://doi.org/10.1007/978-3-030-37371-9_23},
year = {2020}
}

@book{oecd2016ocean,
  title={The ocean economy in 2030},
  author={Oecd},
  year={2016},
  publisher={OECD}
}

@article{Goti-Aralucea2021,
annote = {#serrareadme},
author = {Goti-Aralucea, Leyre and Berkenhagen, J{\"{o}}rg and Ralf, D and Sulanke, Erik},
doi = {10.1016/j.marpol.2021.104675},
file = {:C\:/Users/oerey/Documents/Mendeley-Organized-Literature/2021_Goti-Aralucea et al._Efficiency vs resilience The rise and fall of the German brown shrimp fishery in times of COVID 19.pdf:pdf},
number = {August},
title = {{Efficiency vs resilience : The rise and fall of the German brown shrimp fishery in times of COVID 19}},
volume = {133},
year = {2021}
}

@article{WindSeeG2023,
  author={{Bundeministerium f\"ur Wirtschaft und Klimaschutz}},
  title={Gesetz zur Entwicklung und F\"orderung der Windenergie auf See (Windenergie-auf-See-Gesetz - WindSeeG)
WindSeeG},
  year={2023},
  journal={Bundesgesetzblatt},
  volume={I},
  number=88,
  pages = {1-57},
}

@article{VanDebBurg2019,
        title = {Wrangling Messy {CSV} Files by Detecting Row and Type Patterns},
        author = {{van den Burg}, G. J. J. and Naz{\'a}bal, A. and Sutton, C.},
        journal = {Data Mining and Knowledge Discovery},
        year = {2019},
        volume = {33},
        number = {6},
        pages = {1799--1820},
        issn = {1573-756X},
        doi = {10.1007/s10618-019-00646-y},
}

@article{aviat2011north,
  title={The North Sea brown shrimp fisheries (pp. 5--103)},
  author={Aviat, D and Diamantis, C and Neudecker, T and Berkenhagen, J and M{\"u}ller, M},
  journal={Brussels, Belgium: European Parliament, Policy Department B: Structural and Cohesion Policies},
  year={2011}
}

@article{ICES2019wgcran,
author = "ICES",
title = "{Report of the Working Group on Crangon Fisheries and Life History (WGCRAN)}",
year = "2019",
month = "7",
url = "https://ices-library.figshare.com/articles/report/Report_of_the_Working_Group_on_Crangon_Fisheries_and_Life_History_WGCRAN_/18616955",
doi = "10.17895/ices.pub.8105",
}

@article{rijnsdorp2020different,
  title={Different bottom trawl fisheries have a differential impact on the status of the North Sea seafloor habitats},
  author={Rijnsdorp, AD and Hiddink, Jan Geert and Van Denderen, PD and Hintzen, NT and Eigaard, OR and Valanko, S and Bastardie, F and Bolam, SG and Boulcott, P and Egekvist, J and others},
  journal={ICES Journal of Marine Science},
  volume={77},
  number={5},
  pages={1772--1786},
  year={2020},
  doi={10.1093/icesjms/fsaa050},
  publisher={Oxford University Press}
}

@techreport{RFC4180,
  author = {Y. Shafranovich},
  title = {{Common Format and MIME Type for Comma-Separated Values (CSV) Files}},
  institution = {{The Internet Society}},
  year = 2005,
  pages = {RFC4180},
  doi = {10.17487/rfc4180},
}

@techreport{BALEFishery2022,
  title = {Bericht an die Europa\"aische Kommission nach Artikel 22 der Verordnung (EU) Nr. 1380/2013 \"uber das Gleichgewicht zwischen den Fangkapazit\"aten und den Fangm\"oglichkeiten der deutschen Fischereiflotte im Jahr 2021},
  author = {{Bundesanstalt f\"ur Landwirtschaft und Ern\"ahrung}},
  year = 2022,
}

@article{Janssen2008,
   title = {Towards a Community Framework for Agent-Based Modelling},
   author = {Janssen, Marco A. and Alessa, Lilian Na'ia and Barton, Michael and Bergin, Sean and Lee, Allen},
   journal = {Journal of Artificial Societies and Social Simulation},
   ISSN = {1460-7425},
   volume = {11},
   number = {2},
   pages = {6},
   year = {2008},
   URL = {https://www.jasss.org/11/2/6.html},
   keywords = {Replication, Documentation Protocol, Software Development, Standardization, Test Beds, Education, Primitives},
   abstract = {Agent-based modelling has become an increasingly important tool for scholars studying social and social-ecological systems, but there are no community standards on describing, implementing, testing and teaching these tools. This paper reports on the establishment of the Open Agent-Based Modelling Consortium, www.openabm.org, a community effort to foster the agent-based modelling development, communication, and dissemination for research, practice and education.},
}
