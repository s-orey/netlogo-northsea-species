<!--
SPDX-FileCopyrightText: 2023-2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
-->

# ViNoS - Viable North Sea

> For now, these pages are under development and subject to frequent change and expansion.

## Maps

|              Bathymetry               |                Accessibility                 | Plaiceboxe                           |
| :-----------------------------------: | :------------------------------------------: | ------------------------------------ |
| ![Depth / m](./assets/bathymetry.png) | ![Accessibility](./assets/accessibility.png) | ![Plaicebox](./assets/plaicebox.png) |

|              Bathymetry               |                Accessibility                 | OWF                      |
| :-----------------------------------: | :------------------------------------------: | ------------------------ |
| ![Depth / m](./assets/bathymetry.png) | ![Accessibility](./assets/accessibility.png) | ![OWF](./assets/owf.png) |

|            Traffic            |             Shore proximity              |           Area            |
| :---------------------------: | :--------------------------------------: | :-----------------------: |
| ![unit](./assets/traffic.png) | ![kg km-2](./assets/shore_proximity.png) | ![km2](./assets/area.png) |

|             Shrimp              |             Plaice              |             Sole              |
| :-----------------------------: | :-----------------------------: | :---------------------------: |
| ![kg km-2](./assets/shrimp.png) | ![kg km-2](./assets/plaice.png) | ![kg km-2](./assets/sole.png) |

|                 TBB effort                  |     |
| :-----------------------------------------: | :-: |
| ![kg km-2](./assets/emodnet_tbb_effort.png) |     |

## mkdocs

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Commands

- `mkdocs new [dir-name]` - Create a new project.
- `mkdocs serve` - Start the live-reloading docs server.
- `mkdocs build` - Build the documentation site.
- `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    doc/mkdocs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
