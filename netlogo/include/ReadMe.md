<!--
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
-->

This folder contains NetLogo include files as source code (.nls) or
tabular data.

## NetLogo source code files

action.nls
: methods for the action breed

boat.nls
: methods for the boats breed, uses names from `names.csv`

cache-wms.nls
: downloading and caching of web mapping services

calendar.nls
: basic calendar functionalities using the time extension

ci.nls
: Continuous Integration functions

gear.nls
: methods for the gears breed

geodata.nls
: GIS data functions using the gis extension

import-png.nls
: non-georeferenced imports using the image extension

output.nls
: formatting of the outputs field in the NetLogo GUI

plot.nls
: setup and update of the plot panels in the NetLogo GUI

port.nls
: methods for the ports breed

prey.nls
: methods for the preys breed, some of the data for the preys resides in `prey.csv`

python.nls
: methods to interact with netlogo-python using the python extension

scene.nls
: methods to change the zoom level of the view

time-series.nls
: methods to load time series data (e.g. holidays)

utilities.nls
: collection of many useful functions, often supplementing basic
NetLogo facilities

video.nls
: methods to write out video files
