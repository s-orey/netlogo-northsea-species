; SPDX-FileCopyrightText: 2022-2023 Universität Hamburg
; SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
; SPDX-License-Identifier: Apache-2.0
; SPDX-FileContributor: Sascha Hokamp <sascha.hokamp@uni-hamburg.de>
; SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>

breed [gears gear]

gears-own [
  gear-name
  gear-species
  gear-prey
  gear-width
  gear-drag
  gear-installation-cost
  gear-purchase-cost
  gear-speed             ; speed when trawling
  gear-efficancy         ; efficancy of catching crangon with the specific gear @tofo: use more realistic values
]

globals [
  gear-names
  gear-prey-names
  number-of-gears
  gear-catch-efficancy
]

; We define here a constant and predefined number of gears
to setup-gears
  ask gears [die]

  ; On vessels <300 PS shrimp are typically caught with a 2x10m beam trawl
  ; they typically remain within 25 km of the coast
  create-gears 1 [
    set gear-name "TBB20CSH" ; careful, the number could be mesh size, not beam length
    set gear-species "Shrimp"
    set gear-width .020 ; width of beam in km
    set gear-drag 13 ; some number ... need more realism and unit .. maybe not relevant
    set gear-installation-cost 1000 ; EUR for changing to this gear
    set gear-purchase-cost 25000 ;
    set gear-speed 9.26 ; 5 knots
    set gear-efficancy 0.8
  ]

  ; On vessels <300 PS plaice are typically caught with a 2x4m beam trawl
  ; they typically remain within 25 km of the coast
  create-gears 1 [
    set gear-name "TBB08PLE"
    set gear-species "Plaice"
    set gear-width .008 ; width of beam in km
    set gear-drag 13 ; some number ... need more realism and unit
    set gear-installation-cost 1000 ; EUR for changing to this gear
    set gear-purchase-cost 25000 ;
    set gear-speed 9.26 ; 5 knots
    set gear-efficancy 0.6
  ]

  ; On vessels <300 PS sole are typically caught with a 2x4m beam trawl
  ; they typically remain within 25 km of the coast
  create-gears 1 [
    set gear-name "TBB08SOL"
    set gear-species "Sole"
    set gear-width .008 ; width of beam in km
    set gear-drag 13 ; some number ... need more realism and unit
    set gear-installation-cost 1000 ; EUR for changing to this gear
    set gear-purchase-cost 25000 ;
    set gear-speed 9.26 ; 5 knots
    set gear-efficancy 0.6
  ]

  ; Larger vessels are found throughout the AWZ except plaice box
  create-gears 1 [
    set gear-name "TBB24PLE"
    set gear-species "Plaice"
    set gear-width .024 ; width of beam in km
    set gear-drag 20 ; some number ... need more realism and unit
    set gear-installation-cost 1000 ; EUR for changing to this gear
    set gear-purchase-cost 35000 ; EUR
    set gear-speed 12.96 ; 7 knots
    set gear-efficancy 0.4
  ]
  create-gears 1 [
    set gear-name "TBB24SOL"
    set gear-species "Sole"
    set gear-width .024 ; width of beam in km
    set gear-drag 30 ; some number ... need more realism and unit
    set gear-installation-cost 2000 ; EUR for changing to this gear
    set gear-purchase-cost 50000 ; EUR
    set gear-speed 12.96 ; 7 knots
    set gear-efficancy 0.4
  ]

  ; Larger vessels are found throughout the AWZ
  create-gears 1 [
    set gear-name "OTB60PLE"
    set gear-species "Plaice"
    set gear-width .024 ; width of beam in km
    set gear-drag 20 ; some number ... need more realism and unit
    set gear-installation-cost 1000 ; EUR for changing to this gear
    set gear-purchase-cost 35000 ; EUR
    set gear-speed 9.26 ; 5 knots
    set gear-efficancy 0.4
  ]

  ; Nephrops trawl
  ; data based on Penny 2007 Fisheries Research Services Internal Report No. 01/07
;  create-gears 1 [
;    set gear-name "DRB35NEP"
;    ;set gear-species "Lobster"
;    set gear-species "other" ; as long as not implemented in data file
;    set gear-width .035 ; width of beam in km
;    set gear-drag 1500 ; kgf
;    set gear-installation-cost 1000 ; EUR for changing to this gear
;    set gear-purchase-cost 35000 ; EUR
;    set gear-speed 5 ;
;  ]

  if not any? preys [ error (word "Cannot create gears before preys")]
  ask gears [
    let _gear-prey-name gear-species
    set gear-prey one-of preys with [prey-name = _gear-prey-name]
    if gear-prey = nobody [ die ]
  ]

  ; Report global variables, make sure to keep the order of the gears
  ; and the correct relation between species and gear name

  let my-gear-ids sort [who] of gears
  set number-of-gears count gears
  set gear-names n-values number-of-gears [i -> [gear-name] of gear item i my-gear-ids]
  set gear-prey-names n-values number-of-gears [i -> [gear-species] of gear item i my-gear-ids]
  set gear-catch-efficancy n-values number-of-gears [i -> [gear-efficancy] of gear item i my-gear-ids]

  print (word "Created " count gears " gears: " [gear-name] of gears)

end

to-report gear-prey-indices
  report n-values (count gears) [igear -> position (item igear gear-prey-names) prey-names ]
  print (gear-prey-names)
end
