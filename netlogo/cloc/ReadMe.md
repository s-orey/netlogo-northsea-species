<!--
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
-->

This folder contains a NetLogo source code counter plugin for the
cloc facility available from https://github.com/AlDanial/cloc. NetLogo
was added to cloc in v1.98, but is yet missing from the JOSS generator.
