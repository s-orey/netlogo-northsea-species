<!--
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
SPDX-FileCopyrightText: 2023-2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
-->

This directory contains supplementary data for running the NetLogo model. Here, the
data produced from the species distribution modeling at Thuenen are saved.
