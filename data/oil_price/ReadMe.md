<!--
# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
-->

# Annual oil prices 1986

This folder contains the U.S. Energy Information Administration's (EIA) oil price index built on the West Texas (WTI) barrel price since 1986.
